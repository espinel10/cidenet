CREATE TABLE IF NOT EXISTS employee (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fLastName VARCHAR(20) NOT NULL,
    sLastName VARCHAR(50) NOT NULL,
    fName VARCHAR(20) NOT NULL,
    sName VARCHAR(20) ,
    contry VARCHAR(10) NOT NULL,
    tipoI VARCHAR(5) NOT NULL,
    numI VARCHAR(30) NOT NULL,
    correo VARCHAR(300) NOT NULL,
    fechaIngreso VARCHAR(20) NOT NULL,
    area VARCHAR(20) NOT NULL,
    estado VARCHAR(20) NOT NULL
    );



insert into employee (fLastName ,sLastName,fName,sName,contry,tipoI,numI,correo,fechaIngreso,area,estado) values
('ALEX','MONYOYA','RAFA','DADA','US','CC','12456345a','a@a.com','1-1-2010','RRHH','ACTIVO'),
('ALEXA','MONYOYA','RAFA','DADA','US','CC','1245634s5a','as@a.com','1-1-2010','RRHH','ACTIVO');


CREATE TABLE IF NOT EXISTS movies (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(50) NOT NULL,
minutes INT NOT NULL
);

insert into movies (name, minutes) values
('Dark Knight', 152),
('Memento', 113),
('Matrix', 136);