package com.example.demo.employe.data;

import com.example.demo.employe.model.Employe;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class EmployeRepositoryJdbcTest {

    private EmployeRepositoryJdbc employeRepositoryJdbc;
    private DriverManagerDataSource dataSource;


    @Before
    public void setUp() throws Exception {
        DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
        ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("test-data.sql"));
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        employeRepositoryJdbc=new EmployeRepositoryJdbc(jdbcTemplate);

    }


    @Test
    public void load_all_movies() throws SQLException {

        try {
            Collection<Employe> employes= employeRepositoryJdbc.findAll();
            assertThat(employes,is(Arrays.asList(
                    new Employe(1, "ALEX","MONYOYA","RAFA","DADA","US","CC","12456345a","a@a.com","1-1-2010","RRHH","ACTIVO"),
                    new Employe(2, "ALEXA","MONYOYA","RAFA","DADA","US","CC","1245634s5a","as@a.com","1-1-2010","RRHH","ACTIVO")
            )));


        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    @Test
    public void load_employe_by_id() throws SQLException{
            Employe employe=employeRepositoryJdbc.findById(2);
            System.out.println(employe.toString());
            assertThat(employe,is(new Employe(2, "ALEXA","MONYOYA","RAFA","DADA","US","CC","1245634s5a","as@a.com","1-1-2010","RRHH","ACTIVO")));


    }

    @Test
    public void insert_a_employe () throws SQLException{
        Employe employe =new Employe(3, "ALEXA","MONYOYA","RAFA","DADA","US","CC","1245634s5a","as@a.com","1-1-2010","RRHH","ACTIVO");
        employeRepositoryJdbc.saveOrUpdate(employe);
        Employe employeCargado=employeRepositoryJdbc.findById(3);
        assertThat(employeCargado,is(employe));

    }


}