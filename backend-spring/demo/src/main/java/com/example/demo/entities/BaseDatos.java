package com.example.demo.entities;

import com.example.demo.employe.data.EmployeRepository;
import com.example.demo.employe.data.EmployeRepositoryJdbc;
import com.example.demo.employe.model.Employe;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class BaseDatos {
    private int num=0;
    private int ides=101;
    private List<Empleado> db;
    private List<Edition> edicion;
    private Log logger = LogFactory.getLog(BaseDatos.class);
    //private MovieRepository movieRepository;
    private EmployeRepository employeRepository;
    public int get_ides(){
        return this.ides;
    }


    public BaseDatos(){
        try {
            DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
            ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("creation-database.sql"));
            JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
            employeRepository = new EmployeRepositoryJdbc(jdbcTemplate);

        }catch (Exception e){
            System.out.println(e.getMessage());

        }
        edicion=new ArrayList<Edition>();
        db=new ArrayList<Empleado>();
        List<String> nombres=new ArrayList<>();
        nombres.add("Manuel");
        nombres.add("Leonel");
        nombres.add("Messi");
        nombres.add("James");
        nombres.add("Edgar");
        nombres.add("Allan");
        nombres.add("poe");
        nombres.add("Hesse");
        nombres.add("maximiliano");
        nombres.add("ada");
        nombres.add("ariana");
        nombres.add("veronica");
        nombres.add("daniela");
        nombres.add("catalina");
        nombres.add("benjamin");
        nombres.add("lebrom");
        Random random=new Random();
        for(int i=0; i<100;i++){
        int r=random.nextInt(nombres.size());
        Empleado e=new Empleado();
        e.setId(i+1);
        e.setfName(nombres.get(r).toUpperCase());
        r=random.nextInt(nombres.size());
        e.setsName(nombres.get(r).toUpperCase());
        e.setfLastName(nombres.get(r).toUpperCase());
        e.setsLastName(nombres.get(r).toUpperCase());
        e.setContry("CO");
        e.setTipoI("CC");
        e.setNumI("5115415615");
        e.setFechaIngreso("19/7/1998");
        e.setArea("mercadeo");
        e.setCorreo(generadorCorreo(e));
            Employe e2;
            e2=casting(e);
            employeRepository.saveOrUpdate(e2);

        db.add(e);
        }

    }


    public Employe casting(Empleado e){
        Employe new_eploye=new Employe(e.getId(),e.getfLastName(),e.getsLastName(),e.getfName(),e.getsName(),e.getContry(),e.getTipoI(),e.getNumI(),e.getCorreo(),e.getFechaIngreso(),e.getArea(),e.getEstado());
      return  new_eploye;
    }


    public String generadorCorreo(Empleado e){
        int bandera=0;
        String correo;
        for (Empleado p:db){
            if (e.fLastName==p.fLastName && e.fName==p.fName){
                bandera=1;
                break;
            }

        }
        if (bandera==1){
            correo=e.fName.toLowerCase()+"."+e.fLastName.toLowerCase()+"."+num+"@"+"cidenet.com."+e.contry.toLowerCase();
            this.num=this.num+1;
        }else{
            correo=e.fName.toLowerCase()+"."+e.fLastName.toLowerCase()+"@"+"cidenet.com."+e.contry.toLowerCase();
        }
        return correo;
    }

    public boolean validar_documento(String arr){

        boolean result=true;
        for (Empleado p: db){
            String arr2=p.getTipoI()+":"+p.getNumI();
            if (arr.equals(arr2)){
                result=false;
                break;
            }
        }
        return result;
    }



    public void create_empleado(Empleado e){

        if(validar_documento(e.getTipoI()+":"+e.getNumI())
        ){
            e.setCorreo(generadorCorreo(e));
            e.setId(this.db.size()+1);
            db.add(e);
            logger.info("USUARIO NUEVO EXT"+e.getfName());
            Employe e2;
            e2=casting(e);
            employeRepository.saveOrUpdate(e2);
        }else{
            logger.info("NO SE CREO EL USUARIO EXT"+e.toString());
        }

    }
    public void delete_empleado(int id){
        for (Empleado p: db){
            if (id==p.getId()){
                logger.info("HAS ELIMINADO A EXT"+p.getfName());
                db.remove(p);

                break;
            }
        }
    }

    public Empleado get_empleado(int id){
        Empleado aux=new Empleado();
        for (Empleado p: db){
            if (id==p.getId()){
                aux=p;
                logger.info("LO HAS OBTENIDO EXT"+aux.getfName());
                break;
            }
        }
        return aux;
    }

    public void actualizar_empleado(Empleado e,int id){
        e.setId(id);
        for (Empleado p: db){
            if (id==p.getId()){
                String a=e.getfName()+e.getfLastName()+e.getContry();
                String b=p.getfName()+p.getfLastName()+p.getContry();
                String nuevoCorreo="";
                if (a!=b){
                    nuevoCorreo=generadorCorreo(e);
                }else{
                    nuevoCorreo=e.getCorreo();
                }
                e.setCorreo(nuevoCorreo);
                Edition edit=new Edition(e.id,"28-jul");
                this.delete_empleado(id);
                db.add(e);
                Employe e2;
                e2=casting(e);
                employeRepository.saveOrUpdate(e2);
                edicion.add(edit);
                logger.info("SE ACTUALIZO EL EMPLEADO EXT"+e.getfName());
                break;
            }
        }
    }

    public List<Empleado> getAll(){
        return this.db;
    }
}
