package com.example.demo.employe.data;

import com.example.demo.employe.model.Employe;

import java.util.Collection;

public interface EmployeRepository {
    Employe findById(long id);
    Collection<Employe> findAll();
    void saveOrUpdate(Employe e);

}
