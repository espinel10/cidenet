package com.example.demo.entities;


public class Edition {
    private int id;
    private String fecha;

    public int getId() {
        return id;
    }

    public String getFecha() {
        return fecha;
    }

    public Edition(int id, String fecha) {
        this.id = id;
        this.fecha = fecha;
    }
}
