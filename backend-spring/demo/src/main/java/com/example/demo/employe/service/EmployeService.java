package com.example.demo.employe.service;

import com.example.demo.employe.data.EmployeRepository;
import com.example.demo.employe.model.Employe;

import java.util.Collection;
import java.util.stream.Collectors;

public class EmployeService {
    private EmployeRepository employeRepository;

    public EmployeService(EmployeRepository employeRepository) {
        this.employeRepository = employeRepository;
    }

    public Collection<Employe> findByName(String name){

        return employeRepository.findAll().stream()
                .filter(employe -> employe.getfName()== name).collect(Collectors.toList());

    }

    public Collection<Employe> findMoviesByCorreo(String correo) {
        return employeRepository.findAll().stream()
                .filter(employe -> employe.correo ==correo ).collect(Collectors.toList());
    }


}
