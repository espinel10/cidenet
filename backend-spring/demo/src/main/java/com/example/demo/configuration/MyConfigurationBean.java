package com.example.demo.configuration;

import com.example.demo.bean.MyBean;
import com.example.demo.bean.MyBeanImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfigurationBean {
    @Bean
    public MyBean beanOperation(){
        return new MyBeanImpl();
    }

}
