package com.example.demo.employe.data;

import com.example.demo.employe.model.Employe;
import com.example.demo.entities.BaseDatos;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;

public class EmployeRepositoryJdbc implements EmployeRepository{
    JdbcTemplate jdbcTemplate;
    private Log logger = LogFactory.getLog(EmployeRepositoryJdbc.class);

    public EmployeRepositoryJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Employe findById(long id) {
        Object[] args = { id };
        logger.info("SE ACTUALIZO EL EMPLEADO CON ID EN LA DB INTERNA"+id);
        return jdbcTemplate.queryForObject("SELECT * FROM employee where id = ?",args,employeMapper);
    }

    @Override
    public Collection<Employe> findAll() {
        logger.info("ALL EMPLOYEE IN DB OWN");
        return jdbcTemplate.query("select * from employee",employeMapper);
    }

    @Override
    public void saveOrUpdate(Employe e) {
        logger.info("UPDATE DATA BASE OWN :"+e.correo);
        jdbcTemplate.update("INSERT INTO employee (fLastName ,sLastName,fName,sName,contry,tipoI,numI,correo,fechaIngreso,area,estado) VALUES (?,?,?,?,?,?,?,?,?,?,?)",e.fLastName,e.sLastName,e.fName, e.sName,e.contry,e.tipoI,e.numI,e.correo,e.fechaIngreso,e.area,e.estado);
    }


    private static RowMapper<Employe> employeMapper = (rs, rowNum) ->
            new Employe(
                    rs.getInt("id"),
                    rs.getString("fLastName"),
                    rs.getString("sLastName"),
                    rs.getString("fName"),
                    rs.getString("sName"),
                    rs.getString("contry"),
                    rs.getString("tipoI"),
                    rs.getString("numI"),
                    rs.getString("correo"),
                    rs.getString("fechaIngreso"),
                    rs.getString("area"),
                    rs.getString("estado")
                    );

}
